package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.Config;
import com.example.myapplication.R;
import com.example.myapplication.adapter.Image_Recycler_Adapter;
import com.example.myapplication.model.GetImageModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    Image_Recycler_Adapter image_recycler_adapter;

   @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.recyclerview)
    RecyclerView image_recyclerview;

    GridLayoutManager gridLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        image_recycler_adapter = new Image_Recycler_Adapter(MainActivity.this, Config.getImageModelList);
        gridLayoutManager = new GridLayoutManager(MainActivity.this,3);
        image_recyclerview.setLayoutManager(gridLayoutManager);
        image_recyclerview.setItemAnimator(new DefaultItemAnimator());
        image_recyclerview.setAdapter(image_recycler_adapter);

        json_getimage_request();
    }

    public void json_getimage_request() {

        StringRequest stringRequest=new StringRequest(Request.Method.GET, "https://pixabay.com/api/?key=17353624-d36ef7530b82f2831f75cbe38",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        parsejson(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    public void parsejson(String responce){

        try {
            JSONObject jsonObject=new JSONObject(responce);

            String total=jsonObject.getString("total");
            String totalrequest=jsonObject.getString("totalHits");

            JSONArray hits=jsonObject.getJSONArray("hits");


            for(int i=0;i<hits.length();i++){

                JSONObject jsonObject1=hits.getJSONObject(i);

                String largeImageURL=jsonObject1.getString("largeImageURL");
                String userImageURL=jsonObject1.getString("userImageURL");
                String id=jsonObject1.getString("id");


                GetImageModel getImageModel=new GetImageModel(id,largeImageURL,userImageURL);
                addToImageList(getImageModel, id);

                image_recycler_adapter.notifyDataSetChanged();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private int addToImageList(GetImageModel getImageModel, String shiftId){
        for (int i=0;i<Config.getImageModelList.size();i++){
            GetImageModel getImageModel1=Config.getImageModelList.get(i);
            if (getImageModel1.getId().equals(shiftId)){
                Config.getImageModelList.set(i,getImageModel);
                return 1;
            }
        }
        Config.getImageModelList.add(getImageModel);

        return 0;
    }
}
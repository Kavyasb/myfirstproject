package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.activity.ImageZoomView;
import com.example.myapplication.model.GetImageModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Image_Recycler_Adapter extends RecyclerView.Adapter<Image_Recycler_Adapter.MyViewHolder> {

    List<GetImageModel> getImageModels;
    Context context;
    String userImageURL;

    public Image_Recycler_Adapter(Context context, List<GetImageModel> getImageModels) {
        this.getImageModels = getImageModels;
        this.context=context;
         }

    @Override
    public Image_Recycler_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.activity_image__recycler__adapter, parent, false);
        return new Image_Recycler_Adapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Image_Recycler_Adapter.MyViewHolder holder, int position) {

        final GetImageModel getImageModel=getImageModels.get(position);
        userImageURL=getImageModel.getLargeImageURL();

        Picasso.get()
                .load(userImageURL)
                .resize(300, 300)
                .centerCrop()
                .into(holder.userimage);


        holder.userimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String imageurl=getImageModel.getLargeImageURL();

                Intent i=new Intent(context, ImageZoomView.class);
                i.putExtra("ImageURL",imageurl);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getImageModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView userimage;
        public MyViewHolder(View view) {
            super(view);

            userimage=view.findViewById(R.id.userimage);
        }
    }
}
package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.squareup.picasso.Picasso;

public class ImageZoomView extends AppCompatActivity {

    ImageView imagezoom;
    String imageurl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_zoom_view);
        imagezoom=findViewById(R.id.imagezoom);

        Intent i=getIntent();
        imageurl=i.getStringExtra("ImageURL");

        Picasso.get()
                .load(imageurl)
                .resize(300, 300)
                .centerCrop()
                .into(imagezoom);

    }
}
package com.example.myapplication.model;

public class GetImageModel {

    String id,largeImageURL,userImageURL;

    public GetImageModel(String id,String largeImageURL, String userImageURL){

        this.id=id;
        this.largeImageURL=largeImageURL;
        this.userImageURL=userImageURL;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setLargeImageURL(String largeImageURL) {
        this.largeImageURL = largeImageURL;
    }

    public String getLargeImageURL() {
        return largeImageURL;
    }

    public void setUserImageURL(String userImageURL) {
        this.userImageURL = userImageURL;
    }

    public String getUserImageURL() {
        return userImageURL;
    }
}
